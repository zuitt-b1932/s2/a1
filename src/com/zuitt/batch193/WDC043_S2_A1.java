package com.zuitt.batch193;

import java.util.Scanner;

public class WDC043_S2_A1 {

    public static void main(String[] args){

        Scanner leapScanner = new Scanner(System.in);

        System.out.println("Input year to be checked if it is a leap year.");
        int year = leapScanner.nextInt();

        if((year % 100 == 0) & (year % 400 != 0)) {
            System.out.println(year + " is NOT a Leap Year");
        }
        else if(year % 4 == 0)
           System.out.println(year + " is a Leap Year");
        else
            System.out.println(year + " is NOT a Leap Year");
    }
}
